#!/bin/sh
#AccuWeather (r) RSS weather tool for conky
#
#USAGE: weather.sh <locationcode>
#
#(c) Michael Seiler 2007

METRIC=1 #Should be 0 or 1; 0 for F, 1 for C

if [ -z $1 ]; then
    echo
    echo "USAGE: weather.sh <locationcode>"
    echo
    exit 0;
fi

out=$(curl -s http://rss.accuweather.com/rss/liveweather_rss.asp\?metric\=${METRIC}\&locCode\=$1 | perl -ne 'if (/Currently/) {chomp;/\<title\>Currently: (.*)?\<\/title\>/; print "$1"; }')

cond=`echo $out | cut -d':' -f 1`
temp=`echo $out | cut -d':' -f 2`

case "$cond" in

'Fog')
	cp /home/de-dum_m/Pictures/Conky/weather/Cloudy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Snow')
	cp /home/de-dum_m/Pictures/Conky/weather/Snowy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Cloudy')
	cp /home/de-dum_m/Pictures/Conky/weather/Cloudy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Sunny')
	cp /home/de-dum_m/Pictures/Conky/weather/Sunny.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Partly Sunny')
	cp /home/de-dum_m/Pictures/Conky/weather/Part_sunny.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Partly Cloudy')
	cp /home/de-dum_m/Pictures/Conky/weather/Part_sunny.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Rain')
	cp /home/de-dum_m/Pictures/Conky/weather/Rainy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'Rainy')
	cp /home/de-dum_m/Pictures/Conky/weather/Rainy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
'T-Storms')
	cp /home/de-dum_m/Pictures/Conky/weather/Cloudy.png /home/de-dum_m/Pictures/Conky/weather_icon.png
;;
*)
esac

if [[ $2 == "-p" ]]; then
    echo $out
    echo "# "$cond
fi

echo $temp | sed 's/C/°C/'
